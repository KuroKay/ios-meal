//
//  AppDelegate.swift
//  MyMeals
//
//  Created by Henri LA on 06.12.19.
//  Copyright © 2019 Crea. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
  
  var window: UIWindow? /// Do not touch it
  
  // MARK: Getters
  // For example, these getters are available from UIApplication.appDelegate:
  // UIApplication.appDelegate.networkOperator
  // UIApplication.appDelegate.mealRestAPI

  /// Only for cache !
  private (set) lazy var coreDataStack = CoreDataStack("MyMeals")
  
  private (set) lazy var imageStore = ImageStore()
  
  private (set) lazy var networkOperator: NetworkOperator = {
    let netOperator = NetworkOperator(URLSessionConfiguration.default,
                                      operationQueue: OperationQueue.main,
                                      cache: URLCache())
    return netOperator
  }()
  
  private (set) lazy var mealRestAPI: MealRestAPI = {
    let restEndPoint = RestEndpoint(scheme: "https",
                                    host: "www.themealdb.com",
                                    version: "v1",
                                    apiKey: "1")
    let restAPI = MealRestAPI(restEndpoint: restEndPoint,
                              networkOperator: networkOperator,
                              managedObjectContext: coreDataStack.mainContext)
    return restAPI
  }()
  
  // MARK: Application Lifecycle
  
  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    // Override point for customization after application launch.
    
    if window == nil {
      window = UIWindow(frame: UIScreen.main.bounds)
    }
    
    let tabBarController = UITabBarController()
    
    tabBarController.viewControllers = [UINavigationController(rootViewController: MealCategoriesTableVC()),
                                        UINavigationController(rootViewController: SearchMealsVC())]
    
    window?.rootViewController = tabBarController
    window?.makeKeyAndVisible()
    
    return true
  }

  func applicationWillTerminate(_ application: UIApplication) {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    // try? coreDataStack.saveContext()
  }

}

extension UIApplication {
  static var appDelegate: AppDelegate {
    guard let delegate = UIApplication.shared.delegate as? AppDelegate else {
      fatalError("Do not conform to AppDelegate")
    }
    
    return delegate
  }
}
