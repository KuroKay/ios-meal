//
//  MealsDataSource.swift
//  MyMeals
//
//  Created by Henri LA on 09.12.19.
//  Copyright © 2019 Crea. All rights reserved.
//

import Foundation

class MealsDataSource: GenericDataSource {
  typealias T = Meal
  
  private let mealCategory: MealCategory
  private let mealRestAPI: MealRestAPI
  
  weak var delegate: GenericDataSourceReloadable?
  
  private var dataSource = [Meal]()
  
  init(_ mealCategory: MealCategory, mealRestAPI: MealRestAPI, delegate: GenericDataSourceReloadable){
    self.mealCategory = mealCategory
    self.mealRestAPI = mealRestAPI
    self.delegate = delegate
  }
  
  // MARK: GenericDataSource implementation
  
  func reload() {
    #warning("To complete the implementation")
    // TODO: To complete
  }
  
  func numberOfSections() -> Int {
    return 1
  }
  
  func numberOfItems(in section: Int) -> Int {
    #warning("To complete the implementation")
    // TODO: To complete
    return 0
  }
  
  func item(at indexPath: IndexPath) -> Meal? {
    #warning("To complete the implementation")
    // TODO: To complete
    return nil
  }
  
}
