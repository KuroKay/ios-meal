//
//  MealsTableVC.swift
//  MyMeals
//
//  Created by Henri LA on 10.12.19.
//  Copyright © 2019 Crea. All rights reserved.
//

import UIKit

class MealsTableVC: UITableViewController {
  
  private lazy var dataSource = MealsDataSource(mealCategory, mealRestAPI: UIApplication.appDelegate.mealRestAPI, delegate: self)
  
  // MARK: Variables configuration
  
  private let mealCategory: MealCategory
  
  // MARK: Init
  
  init(mealCategory: MealCategory) {
    self.mealCategory = mealCategory
    super.init(nibName: nil, bundle: nil)
    
    #warning("To complete the implementation")
    // TODO: Set the title
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  // MARK: Lifecycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    #warning("To complete the implementation")
    // TODO: Create a UIRefreshControl and assign it to 'refreshControl' of tableView
    // 1. Hint: refresControl = ...
    // 2. The UIRefreshControl tint color has to be purple !
    // 3. The UIRefreshControl need to addTarget -> self, #selector(reload), .valueChanged
    
    // TODO: Register the tableViewCell you created
    
    dataSource.reload()
  }
  
  @objc private func reload(){
    #warning("To complete the implementation")
    
    dataSource.reload()
  }
  
  
  // MARK: Table view data source
  
  override func numberOfSections(in tableView: UITableView) -> Int {
    #warning("To complete the implementation")
    return 0
  }
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    #warning("To complete the implementation")
    return 0
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    #warning("To complete the implementation")
    return UITableViewCell()
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    #warning("To complete the implementation")
    // TODO: Get the MealDetails according to the selected row
    // Hint: In order to do it, you need to use MealRestAPI !
    // Hint: In the completion handler -> call handleMealDetailsResponse(...)
    
  }
  
  // MARK: Handle response
  
  private func handleMealDetailsResponse(_ mealDetailsResponse: MealDetailsResponse, at indexPath: IndexPath) {
    #warning("To complete the implementation")
    // TODO: Check in mealDetailsResponse, if there is an error. If there is, display ONLY UIAlertViewController
    // TODO: If there is no error, check if mealDetailsResponse has a MealDetails.
    //       - If there is one, create a ModalFullScreenNavigationVC by passing a MealDetailsVC
    //          - Present the created ModalFullScreenNavigationVC in the closure below !
    DispatchQueue.main.async { //[weak self] in
      // self?.present(... use the auto complete...)
    }
    
    // TODO: Deselect the cell
  }
  
}

extension MealsTableVC: GenericDataSourceReloadable {
  
  func dataSourceDidReload(with error: Error?) {
    #warning("Fix it - there is something wrong below")
    if let error = error {
      let alertController = UIAlertController(title: "Error",
                                              message: error.localizedDescription,
                                              preferredStyle: .alert)
      alertController.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
      present(alertController, animated: true, completion: nil)
    }
    
    tableView.reloadData()
  }
  
}
