//
//  MealCategoriesTableVC.swift
//  MyMeals
//
//  Created by Henri LA on 09.12.19.
//  Copyright © 2019 Crea. All rights reserved.
//

import UIKit

class MealCategoriesTableVC: UITableViewController {
  
  // MARK: Variables configuration
  
  private lazy var mealCategoriesDataSource = NoIndexMealCategoriesDataSource(mealRestAPI: UIApplication.appDelegate.mealRestAPI,
                                                                       reloadDelegate: self)
  #warning("When MealCategoriesDataSource is fixed - comment the line above and uncomment the line below")
 private lazy var mealCategoriesDataSource = MealCategoriesDataSource(mealRestAPI: UIApplication.appDelegate.mealRestAPI,reloadDelegate: self)
  
  // MARK: Init
  
  init() {
    super.init(nibName: nil, bundle: nil)
    
    title = "Meals Categories"
    
    tabBarItem = UITabBarItem(title: title, image: UIImage(named: "tab1"), tag: 0)
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  // MARK: Lifecycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    tableView.register(UITableViewCell.self, forCellReuseIdentifier: "UITableViewCell")
    
    mealCategoriesDataSource.reload()
  }
  
  // MARK: TableView data source & delegate
  
  override func numberOfSections(in tableView: UITableView) -> Int {
    return 3
  }
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 5
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "UITableViewCell", for: indexPath)
    
    if let item = mealCategoriesDataSource.item(at: indexPath) {
      cell.textLabel?.text = item.strCategory
    }
    
    return cell
  }
  
  override func sectionIndexTitles(for tableView: UITableView) -> [String]? {

  }
  
  override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    return self.meal.count
    return nil
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let testVC = TestViewController(nibName : "TestViewController" , bundle:nil)
    navigationController?.pushViewController(testVC, animated: true)

    print("You have selected \(indexPath.row) row")
  }
  
}

// MARK: GenericDataSourceReloadable implementation for MealCategoriesTableVC

extension MealCategoriesTableVC: GenericDataSourceReloadable {
  
  func dataSourceDidReload(with error: Error?) {

    let alert = UIAlertController(title: "My Alert", message: "This is an alert.", preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
    NSLog("The \"OK\" alert occured.")
    }))
    self.present(alert, animated: true, completion: nil)
    
    tableView.reloadData()
  }
  
}
