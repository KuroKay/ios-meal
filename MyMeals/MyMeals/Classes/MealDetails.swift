//
//  Meal.swift
//  Meals
//
//  Created by Henri LA on 05.12.19.
//  Copyright © 2019 Crea. All rights reserved.
//

import Foundation

struct MealDetails: Codable {
  
  let idMeal: String
  let strMeal: String?
  let strDrinkAlternate: String?
  let strCategory: String?
  let strArea: String?
  let strInstructions: String?
  let strMealThumb: String?
  let strTags: String?
  let strYoutube: String?
  let strSource: String?
  let dateModified: String?
  
  init(_ dictionary: [String: Any]) {
    idMeal = dictionary["idMeal"] as? String ?? ""
    strMeal = dictionary["strMeal"] as? String
    strDrinkAlternate = dictionary["strDrinkAlternate"] as? String
    strCategory = dictionary["strCategory"] as? String
    strArea = dictionary["strArea"] as? String
    strInstructions = dictionary["strInstructions"] as? String
    strTags = dictionary["strTags"] as? String
    strMealThumb = dictionary["strMealThumb"] as? String
    strYoutube = dictionary["strYoutube"] as? String
    strSource = dictionary["strSource"] as? String
    dateModified = dictionary["dateModified"] as? String
  }
  
  var mealTags: [String] {
    var tags = [String]()
    tags.append("strTags")
    return []
  }
    
    
    var imageURL: URL? {
        let result: URL?
        if let thumbStringURL = strMealThumb {
            result = URL(string: thumbStringURL)
        } else {
            result = nil
        }
    return result
  }
  
}

extension MealDetails {
  
  static func meal(from responseResult: ResponseResult) -> MealDetailsResponse {
    var result: MealDetailsResponse
    result.mealDetails = nil
    result.error = nil
    
    result.1 = responseResult.error
    
    // TODO: Set result.mealDetails -> it expects a MealDetails
    // Hint: 1.Get the data 2. Create a JSON (Dictionary/Array) and create MealDetails and assign it to
    //       result.mealDetails = ...
     if let data = responseResult.data,
         let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any],
         let mealDetails = json["mealDetails"] as? [[String: String]]
       {
         var dataResult = [MealDetails]()
         
         mealDetails.forEach{ dataResult.append(MealDetails(dictionary: $0)) }
         
         result.mealDetails = dataResult
       }
       
       return result
     }
  
  static func meals(from responseResult: ResponseResult) -> SearchedMealsDetailsResponse {
    var result: SearchedMealsDetailsResponse
    result.arrayMealDetails = []
    result.error = nil
    
    #warning("To complete the implementation based on MealCategory example from the func: mealCategories")
    // TODO: Set error
    
    // TODO: Set result.arrayMealDetails -> it expects an [MealDetails]
    // Hint: 1.Get the data 2. Get the Array from "categories" 3. Loop and create a MealCategory
    
    return result
  }
  
}
