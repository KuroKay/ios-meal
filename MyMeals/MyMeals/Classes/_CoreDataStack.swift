import CoreData

public enum PersistentStoreOptions {
  public static let `default` = [NSMigratePersistentStoresAutomaticallyOption: true]
  public static let migration = [NSMigratePersistentStoresAutomaticallyOption: true,
                                 NSInferMappingModelAutomaticallyOption: true]
}

public final class CoreDataStack {
  // MARK: - Private constants
  
  private let modelName: String
  private let bundle: Bundle
  private let storeType: String
  private let storeOptions: [String: Bool]
  
  // MARK: - Public variable
  
  public private(set) var mainContext: NSManagedObjectContext
  public private(set) var privateContext: NSManagedObjectContext
  
  // MARK: - Private lazy variables
  
  private lazy var psc: NSPersistentStoreCoordinator = {
    let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
    
    let url = self.applicationDocumentsDirectory
      .appendingPathComponent("\(self.modelName).sqlite")
    
    do {
      
      try coordinator.addPersistentStore(ofType: storeType, configurationName: nil, at: url, options: storeOptions)
    } catch {
      var dict = [String: AnyObject]()
      dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject?
      dict[NSLocalizedFailureReasonErrorKey] = "There was an error creating or loading the application's saved data." as AnyObject?
      
      dict[NSUnderlyingErrorKey] = error as NSError
      let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
      print(wrappedError)
    }
    
    return coordinator
  }()
  
  private lazy var managedObjectModel: NSManagedObjectModel = {
    guard let modelURL = bundle.url(forResource: self.modelName, withExtension: "momd") else {
      fatalError("Unable to find : \(self.modelName).momd")
    }
    guard let managedObject = NSManagedObjectModel(contentsOf: modelURL) else {
      fatalError("Unable to init NSManagedObjectModel")
    }
    return managedObject
    
  }()
  
  private lazy var applicationDocumentsDirectory: URL = {
    let urls = FileManager.default.urls(
      for: .documentDirectory, in: .userDomainMask
    )
    return urls[urls.count - 1]
  }()
  
  // MARK: - Inits
  
  public init(_ modelName: String,
              _ bundle: Bundle = Bundle.main,
              _ storeType: String = NSSQLiteStoreType,
              _ storeOptions: [String: Bool] = PersistentStoreOptions.migration) {
    self.modelName = modelName
    self.bundle = bundle
    self.storeType = storeType
    self.storeOptions = storeOptions
    
    mainContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
    privateContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
    
    mainContext.persistentStoreCoordinator = psc
    mainContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
    privateContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
    
    privateContext.parent = mainContext
  }
  
  // MARK: - Public function
  
  /// Save the coreData context (Private and main Context)
  public func saveContext() throws {
    if privateContext.hasChanges {
      do {
        try privateContext.save()
      } catch let error as NSError {
        print(error.localizedDescription)
        throw error
      }
    }
    
    if mainContext.hasChanges {
      do {
        try mainContext.save()
      } catch let error as NSError {
        print(error.localizedDescription)
        throw error
      }
    }
  }
  
}
